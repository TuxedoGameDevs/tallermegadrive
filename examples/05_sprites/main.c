#include "main.h"
#include "snd/psg/sfxbank.h"

////////////////////////////////////////////////////////////////////////////////
//
// int main()
//
////////////////////////////////////////////////////////////////////////////////
int main(){
	
	SpriteDef skullSprite[4];
	u8 a, b;
	u16 posX = 96, posY = 80, keys = 0;
	
	VDP_regInit();
	
	// modo de 32 columnas
	VDP_setReg(12, 0x00 );
	
	// cambia el color del fondo
    VDP_setReg( 7, 0x0F );
	
	// carga el driver XGM en el z80
    SND_loadDriver_XGM();
	
    VDP_loadTileData( font00_tiles, pos_vram_font, font_tiles_size, 1);
	
	VDP_drawText("sprites",BPLAN, TILE_ATTR_FULL(PAL0, 1, 0, 0,  pos_vram_font), 12, 8 );
	
	// cargamos en vram los tiels del sprite
	VDP_loadTileData( sprite_tiles, 1, sprite_tiles_size, 1);
	
	// inicializacion de los sprites ( 2x2 de 32x32 )
	for( b = 0; b < 2; b++){
        for( a = 0; a < 2; a++){
            skullSprite[a+b*2].posx = posX+32*a;
			skullSprite[a+b*2].posy = posY+32*b;
            skullSprite[a+b*2].size = SPRITE_SIZE( 4, 4 );
            skullSprite[a+b*2].tile_attr = TILE_ATTR_FULL( PAL1, 1, 0, 0, 1+16*a+32*b ); // paleta, prioridad, vflip, hflip, tile 1
            skullSprite[a+b*2].link  = a+b*2+1;
            VDP_setSpriteP( a+2*b, &skullSprite[a+2*b]);
        }
	}
	
	VDP_setPalette((u16 *)font00_pal, 0, 16);
	VDP_setPalette((u16 *)sprite_pal, 16, 16);
	
    ////////////////////////////////////////
    //  bucle infinito
	while(1){
		
		// lee el pad
		keys = get_pad(0) & SEGA_CTRL_BUTTONS;

		if(  keys & SEGA_CTRL_UP )	posY--;
		else if(  keys & SEGA_CTRL_DOWN )	posY++;
		
		if(  keys & SEGA_CTRL_LEFT )	posX--;
		else if(  keys & SEGA_CTRL_RIGHT )	posX++;
		
		for( b = 0; b < 2; b++)
			for( a = 0; a < 2; a++)
				VDP_setSpritePosition( a+2*b, posX+32*a, posY+32*b);

		VDP_waitVSync();
	}

    return 0;
}
