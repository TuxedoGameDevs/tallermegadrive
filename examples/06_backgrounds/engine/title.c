#include "../main.h"

//////////////////////////////////////////////////////////////////////////////////////
//
// void draw1985Logo()
// dibuja el logo de 1985
//
//////////////////////////////////////////////////////////////////////////////////////
void draw1985Logo(){
	
	// para la descompresion
    u16 size;

   // reseteo las capas, los sprites
    VDP_resetSprites();
    VDP_updateSprites();

    //clear_screen();
    VDP_clearPlan(APLAN);
    VDP_clearPlan(BPLAN);

    VDP_setVerticalScroll(APLAN, 0, 0);
	VDP_setVerticalScroll(BPLAN, 0, 0);

    // cargo la paleta inicial
    VDP_setPalette((u16 *)palette_black, 0, 16);
    VDP_setPalette((u16 *)palette_black, 16, 16);
    VDP_setPalette((u16 *)palette_black, 32, 16);
    VDP_setPalette((u16 *)palette_black, 48, 16);

    // cambia el color del fondo
    VDP_setReg(7, 0x05);
	
	// cargo los tiles comprimidos del logo
	size = depack( logo1985_data, temp );
    VDP_vramCopy( 0, &temp[16], size-32 ); // dir destino= tile*32, origen, tamaño
    VDP_unpackPalette( palA );

    // dibuja el mapa
    VDP_fillTileMap( APLAN, logo1985_map, 8, 8, 16, 10, TILE_ATTR_FULL(PAL0, 1, 0, 0, 1));  // plano, mapa, x, y, ancho, alto, atributos de tile
		
    // fade in de paleta
    VDP_initFading1( 0, 15, palette_black, palA, 30 );    // from col, to col, pal src, pal dst, numframes);
    while(VDP_doStepFading1()) VDP_waitVSync();
    
	VDP_setPalette((u16 *)palA, 0, 16);

    // espera
    delay(120);

    // fade out
    VDP_initFading1(0, 15, palA, palette_black, 30);    // from col, to col, pal src, pal dst, numframes);
    while(VDP_doStepFading1()) VDP_waitVSync();
}

///////////////////////////////////////////////////////////////////////////////////////
//
//  pantalla de records
//
///////////////////////////////////////////////////////////////////////////////////////
u8 recordsScreen( u8 numPlayers ){
	
	
    return 1;
}

//////////////////////////////////////////////////////////////////////////////////////
//
//  pantalla de titulo
//
//////////////////////////////////////////////////////////////////////////////////////
u8 titleScreen(){

  return 0;
}

//////////////////////////////////////////////////////////////////////////////////////
//
//  la intro del juego
//
//////////////////////////////////////////////////////////////////////////////////////
u8 introScreen(){
	
	
    return 0;
}

////////////////////////////////////////////////////////
//
// void creditsScreen(){
// creditos iniciales del juego
//
////////////////////////////////////////////////////////
void creditsScreen(){

    // reseteo las capas, los sprites
    VDP_resetSprites();
    VDP_updateSprites();

    VDP_clearPlan(APLAN);
    VDP_clearPlan(BPLAN);

    // cambia el color del fondo
    VDP_setReg(7, 0x0E);

    // cargo la paleta inicial
    VDP_setPalette((u16 *)palette_black, 0, 16);
    VDP_setPalette((u16 *)palette_black, 16, 16);
    VDP_setPalette((u16 *)palette_black, 32, 16);
    VDP_setPalette((u16 *)palette_black, 48, 16);

    VDP_loadTileData( font00_tiles, pos_vram_font, font_tiles_size, 1);
	
	VDP_drawText("CODE ",BPLAN, TILE_ATTR_FULL(PAL1, 1, 0, 0,  pos_vram_font), 7, 2 );

    VDP_drawText("GFX. ", BPLAN, TILE_ATTR_FULL(PAL1, 1, 0, 0,  pos_vram_font), 7, 5 );
    VDP_drawText("        ", BPLAN, TILE_ATTR_FULL(PAL1, 1, 0, 0,  pos_vram_font), 7, 7 );
	VDP_drawText("     ", BPLAN, TILE_ATTR_FULL(PAL1, 1, 0, 0,  pos_vram_font), 7, 9 );

    VDP_drawText("MUSIC ",BPLAN, TILE_ATTR_FULL(PAL1, 1, 0, 0,  pos_vram_font), 7, 12 );

    VDP_drawText("ILLUST. ",BPLAN, TILE_ATTR_FULL(PAL1, 1, 0, 0,  pos_vram_font), 7, 15 );

    VDP_drawText("thanks to ",BPLAN, TILE_ATTR_FULL(PAL1, 1, 0, 0,  pos_vram_font), 1, 20);
    VDP_drawText(" and YOU",BPLAN, TILE_ATTR_FULL(PAL1, 1, 0, 0,  pos_vram_font), 3, 22 );

    VDP_drawText("NOT PRODUCED BY OR UNDER LICENSE",BPLAN, TILE_ATTR_FULL(PAL1, 1, 0, 0,  pos_vram_font), 0, 25);
    VDP_drawText("   FROM SEGA ENTERPRISES LTD",BPLAN, TILE_ATTR_FULL(PAL1, 1, 0, 0,  pos_vram_font), 0, 26);

    // fade in de paleta
    VDP_initFading1(16, 31, palette_black, font00_pal, 30);    // from col, to col, pal src, pal dst, ... , numframes);
    while(VDP_doStepFading1())
        VDP_waitVSync();
    VDP_setPalette((u16 *)font00_pal, 16, 16);

    delay(60*5);

    VDP_initFading1(16, 31, font00_pal, palette_black, 30);    
    while(VDP_doStepFading1())
        VDP_waitVSync();
}
