#include "main.h"
#include "snd/psg/sfxbank.h"

////////////////////////////////////////////////////////////////////////////////
//
// int main()
//
////////////////////////////////////////////////////////////////////////////////
int main(){
	
	u8 numPlayers = 0;
	
    u8 exit = 0;

    // Pone las copias "shadow" de los registros a sus
    // valores por defecto, establecidos en hw_md.s
    VDP_regInit();
	
	// modo de 32 columnas
    VDP_setReg(12, 0x00);

    // carga el driver XGM en el z80
    SND_loadDriver_XGM();
	
	// inicio el banco de sonidos psg
    psgFxInit(sfxbank_data);

	// inicia la tabla de records
	initRecords();
	
	// pantalla de creditos
    creditsScreen();

    ////////////////////////////////////////
    //  bucle infinito
    ////////////////////////////////////////
	while(1){

	    // attract mode
	    while( !exit ){

            draw1985Logo();     // pantalla del logo

            introScreen();    // intro

            // pantalla de titulo
            exit = titleScreen();
			numPlayers = exit;

            // atract
            if(!exit)
                recordsScreen(0);
	    }

        // doy valor inicial a las vars del juego,
        // carga los tiles e inicia sus sprites
        initGame( numPlayers );

        gameLoop( numPlayers );
        recordsScreen(1);

        exit = 0;
	}

    return 0;
}
