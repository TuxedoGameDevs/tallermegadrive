#include "..\main.h"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// bucle del juego
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void gameLoop( u8 numPlayers ){
	
	u8 exit = 0, pause = 0, a = 0;
	unsigned int keysPlayer[maxNumPlayers];
	u8 keyPressedP1 = 0;
	
	initStage( numPlayers );
	
	while( !exit ){
	
		// lee el pad
		keysPlayer[0] = get_pad(0) & SEGA_CTRL_BUTTONS;
		keysPlayer[1] = get_pad(1) & SEGA_CTRL_BUTTONS;

		///////////////////////////////////////////////////////////////////////
        // si no estoy en pausa 
        if( !pause ){
			
			////////////////////////////////////////////////////////////////////
			// ejecuto la logica juego
			

            ////////////////////////////////////////////////////////////////////
            // condicion de game over y salida
           /* if( !player[0].lives && !player[1].lives ){
                   
                gameOverScreen();
				recordsScreen( numPlayers );	
                exit = 1;
            }*/

            //////////////////////////////////////////////////////////////////
            // condicion de cambio de fase
			/*if( player[0].exit ){

				// si estamos en la ultima fase
				if( stage == lastStage ){

					endScreen();
					recordsScreen( numPlayers );
					exit = 1;
				}
				// si no, cambio de mundo
				else
					stage++;

				// cambio de fase
				if( exit != 1 )
					initStage( numPlayers );
			}*/
        }
		// menu de pausa
		else{
				
            a++;
            if( a == 10 )
                a = 0;

            // parpadeo del boton de pausa
            if( a < 5 )
				VDP_drawText( "-PAUSED-", APLAN, TILE_ATTR_FULL(PAL0, 1, 0, 0,  pos_vram_font), 12, 14 );
            else
				VDP_drawText( "        ", APLAN, TILE_ATTR_FULL(PAL0, 1, 0, 0,  pos_vram_font), 12, 14 );
        }

        // control de la pausa
        if(( keysPlayer[0] & SEGA_CTRL_START ) && !keyPressedP1  ){
            
            if( !pause ){
                pause = 1;
            }
            else{
                pause = 0;
				VDP_drawText( "        ", APLAN, TILE_ATTR_FULL(PAL0, 1, 0, 0,  pos_vram_font), 12, 14 );
            }
			
			keyPressedP1 = 1;
		}
		
		if( !( keysPlayer[0] & SEGA_CTRL_START ) )
			keyPressedP1 =  0;
			
        // sincroniza la pantalla
		VDP_waitVSync();
	}
}

