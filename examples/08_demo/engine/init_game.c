#include "../main.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// valores iniciales de la pantalla de records
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
//
// valores iniciales de la pantalla de records
//
/////////////////////////////////////////////////////////////////
void initRecords(){

    u8 a;

    for( a = 0; a < 3; a++ ){
        strcpy(namesTable[a][0], "PLAYER...");
        strcpy(namesTable[a][1], "PLAYER...");
        strcpy(namesTable[a][2], "PLAYER...");
        strcpy(namesTable[a][3], "PLAYER...");
        strcpy(namesTable[a][4], "PLAYER...");
        strcpy(namesTable[a][5], "PLAYER...");
        strcpy(namesTable[a][6], "PLAYER...");
        strcpy(namesTable[a][7], "PLAYER...");
        strcpy(namesTable[a][8], "PLAYER...");
        strcpy(namesTable[a][9], "PLAYER...");


        pointsTable[a][0] = 30000;
        pointsTable[a][1] = 20000;
        pointsTable[a][2] = 10000;
        pointsTable[a][3] = 7500;
        pointsTable[a][4] = 5000;
        pointsTable[a][5] = 2500;
        pointsTable[a][6] = 2000;
        pointsTable[a][7] = 1500;
        pointsTable[a][8] = 1000;
        pointsTable[a][9] = 500;

        ranksTable[a][0] = a;
        ranksTable[a][1] = a;
        ranksTable[a][2] = a;
        ranksTable[a][3] = a;
        ranksTable[a][4] = a;
        ranksTable[a][5] = a;
        ranksTable[a][6] = a;
        ranksTable[a][7] = a;
        ranksTable[a][8] = a;
        ranksTable[a][9] = a;
    }

    // Inicializar logros a 0
    ad.achievements = 0;

    // Logro 18 conseguido!
    //ad.achiev18 = 1;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// doy valor inicial a las variables del juego (personajes...)
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void initGame( u8 numPlayers ){

	// jugador
    player.x = FIX32(32);
    player.y = FIX32(160);
    player.mapX = FIX32(32); 
    player.mapY = FIX32(160);  
    player.speedX = player.speedY = 0;

    player.frame = stand_frame;
	player.dir = dirRight;
    player.state = playerStand;
	player.allowJump = TRUE;

	// carga de los tiles de la fuente
	VDP_loadTileData( font00_tiles, pos_vram_font, font_tiles_size, 1);
	
	// cargo los tiles del fondo A , B y sprite en la vram 
    VDP_loadTileData( bgB_tiles, bgB_tiles_pos, bgB_tiles_size, 1);
	VDP_loadTileData( bgA_tiles, bgA_tiles_pos, bgA_tiles_size, 1);
	VDP_loadTileData( spr_tiles[0], spr_tiles_pos, spr_tiles_size, 1);
	
	// dibujo el mapa de tiles en el plano indicado, x, y, ancho, alto, atributos de tile 
	VDP_fillTileMap( BPLAN, bgB_map, 0, 0, 64, 28, TILE_ATTR_FULL(PAL1, 1, 0, 0, 1));
	
    // dibuja el mapa de tiles desde x1, y1 a x2, y2, del ancho total en tiles, atributos de tile
    VDP_fillBigTileMap( APLAN, bgA_map, 0, 0, screen_width_tiles, mapSizeY, mapSizeX, TILE_ATTR_FULL(PAL2, 1, 0, 0, bgA_tiles_pos));
	
	// sprite del jugador
    player.sprite.posx = fix32ToInt(player.x);
    player.sprite.posy = fix32ToInt(player.y);
    player.sprite.size = SPRITE_SIZE( 4, 4 );
    player.sprite.tile_attr = TILE_ATTR_FULL( PAL3, 1, 0, 0, spr_tiles_pos ); // paleta, prioridad, vflip, hflip, tile 1
    player.sprite.link  = player_sprite;
    VDP_setSpriteP( player_sprite, &player.sprite);
	
	SND_loadSong_XGM(0);
	
	XGM_setMusicTempo(50);
	
	VDP_initFading4( 0, 15, palette_black, font00_pal, 16, 31, palette_black, bgB_pal, 32, 47, palette_black, bgA_pal, 48, 63, palette_black, spr_pal, 30 );    // from col, to col, pal src, pal dst, numframes);
	while( VDP_doStepFading4())
		VDP_waitVSync();
	
	VDP_setPalette((u16 *)font00_pal, 0, 16);
	VDP_setPalette((u16 *)bgB_pal, 16, 16);
	VDP_setPalette((u16 *)bgA_pal, 32, 16);
	VDP_setPalette((u16 *)spr_pal, 48, 16);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// doy valor inicial a las variables del juego (personajes...)
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void initStage( u8 numPlayers ){
	
	// posicion de los scroll del mapa grande
    scrollPosXPlanA = scrollPosXPlanB = 0;
}

