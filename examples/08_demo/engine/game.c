#include "../main.h"


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// bucle del juego
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void gameLoop( u8 numPlayers ){
	
	u8 exit = 0, pause = 0, a = 0;
	u8 keysPlayer[maxNumPlayers];
	u8 keyPressedP1 = 0;
	u8 ticks = 0;

	initStage( numPlayers );
	
	while( !exit ){
	
		// lee el pad
		keysPlayer[0] = get_pad(0) & SEGA_CTRL_BUTTONS;

		///////////////////////////////////////////////////////////////////////
        // si no estoy en pausa 
        if( !pause ){
			
			////////////////////////////////////////////////////////////////////
			// ejecuto la logica juego
			playerControl( ticks, keysPlayer );

            ticks++;
            ticks %= 10;

            ////////////////////////////////////////////////////////////////////
            // condicion de game over y salida
           /* if( !player[0].lives && !player[1].lives ){
                   
                gameOverScreen();
				recordsScreen( numPlayers );	
                exit = 1;
            }*/

            //////////////////////////////////////////////////////////////////
            // condicion de cambio de fase
			/*if( player[0].exit ){

				// si estamos en la ultima fase
				if( stage == lastStage ){

					endScreen();
					recordsScreen( numPlayers );
					exit = 1;
				}
				// si no, cambio de mundo
				else
					stage++;

				// cambio de fase
				if( exit != 1 )
					initStage( numPlayers );
			}*/
        }
		// menu de pausa
		else{
				
            a++;
            if( a == 10 )
                a = 0;

            // parpadeo del boton de pausa
            if( a < 5 )
				VDP_drawText( "-PAUSED-", APLAN, TILE_ATTR_FULL(PAL0, 1, 0, 0,  pos_vram_font), 12, 14 );
            else
				VDP_drawText( "        ", APLAN, TILE_ATTR_FULL(PAL0, 1, 0, 0,  pos_vram_font), 12, 14 );
        }

        // control de la pausa
        if(( keysPlayer[0] & SEGA_CTRL_START ) && !keyPressedP1  ){
            
            if( !pause ){
                pause = 1;
				SND_stopPlay_XGM();
            }
            else{
                pause = 0;
				SND_resumePlay_XGM();
				VDP_drawText( "        ", APLAN, TILE_ATTR_FULL(PAL0, 1, 0, 0,  pos_vram_font), 12, 14 );
            }
			
			keyPressedP1 = 1;
		}
		
		if( !( keysPlayer[0] & SEGA_CTRL_START ) )
			keyPressedP1 =  0;
			
        // sincroniza la pantalla
		VDP_waitVSync();
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// control del jugador
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
u8 playerControl( u8 ticks, u8 keysPlayer[] ){

    u8 salir = 0;
    u8 frame_id = 0; 
	
	u8 collision = 0; 
	
	collision = playerCollisionMapDown();
	
    // jugador en salto
    if (( collision != COLLISION_TILE ) || ( player.state >= playerJump && player.state < playerJumpEnd )){

        // desplazamiento en y
		if( player.speedY <maxFallSpeed )
			player.speedY = fix32Add( player.speedY, gravity );
		
        player.y = fix32Add( player.y, player.speedY );
        player.mapY = fix32Add( player.mapY, player.speedY );

        // desplazamiento en x derecha
        if( player.state == playerJumpRight || player.state == 8 )
            movePlayerRight( speedRun );

        // izquierda
        if( player.state == playerJumpLeft || player.state == 9 )
            movePlayerLeft( speedRun );

        ///////////////////////////////////////
        // empezamos a caer, cambio los tiles
        if( player.speedY >= FIX32(0) && player.state < 7 ){
            player.state += 3;
			VDP_loadTileData( spr_tiles[jump2_frame], spr_tiles_pos, spr_tiles_size, 1);
        }
		
        ////////////////////////////////////////////////////////
        // correcion del salto en vuelo
        if( keysPlayer[0] & SEGA_CTRL_RIGHT ){

            // cambiamos el sentido del sprite una unica vez
            if( player.dir != dirRight){
                player.dir = dirRight;
                player.speedX = 0;
				
				player.sprite.tile_attr = TILE_ATTR_FULL( PAL3, 1, 0, player.dir, spr_tiles_pos ); 
				VDP_setSpriteP( player_sprite, &player.sprite);
            }

            // si no es el salto hacia la derecha
            if( player.state != playerJumpRight && player.state != 8 ){
                player.speedX = fix32Add( player.speedX, jumpCorrection );
				movePlayerRight( player.speedX );
            }
        }

        else if( keysPlayer[0] & SEGA_CTRL_LEFT ){

            // cambiamos el sentido del sprite una unica vez
            if( player.dir != dirLeft ){
                player.dir = dirLeft;
                player.speedX = 0;
                
				player.sprite.tile_attr = TILE_ATTR_FULL( PAL3, 1, 0, player.dir, spr_tiles_pos ); 
				VDP_setSpriteP( player_sprite, &player.sprite);
            }

            // si no es el salto hacia la izquierda
            if( player.state != playerJumpLeft && player.state != 9 ){
                player.speedX = fix32Add( player.speedX, jumpCorrection );
                movePlayerLeft( player.speedX );
            }
        }

		///////////////////////////////////////////////////////////////////////////////////////////
        // estamos cayendo y tocamos el suelo
        if( DOWNMOVEMENT( player.speedY ) && FLOORCOLLISION( collision )){
			
			// (macro) Reajustamos la posicion a múltiplo de 8
			tileAdjust( player.y );
            tileAdjust( player.mapY );
			
			player.speedX = player.speedY = 0;
            player.frame = stand_frame;
            player.state = playerJumpEnd;   // volvemos al estado inicial
			VDP_loadTileData( spr_tiles[stand_frame], spr_tiles_pos, spr_tiles_size, 1);
        }

		VDP_setSpritePosition( player_sprite, fix32ToInt( player.x ), fix32ToInt( player.y ));
    }
    
	// jugador en el suelo
    if( player.state < playerJump || player.state == playerJumpEnd ){
	
        // pulsado ABAJO
        if( keysPlayer[0] & SEGA_CTRL_DOWN ){

            // cambio el sprite
            if( player.state != playerCrouch ){
                player.state = playerCrouch;
				VDP_loadTileData( spr_tiles[crouch_frame], spr_tiles_pos, spr_tiles_size, 1);
            }

            // cambio el flip
            if( keysPlayer[0] & SEGA_CTRL_RIGHT && player.dir != dirRight ){
                player.dir = dirRight;

				player.sprite.tile_attr = TILE_ATTR_FULL( PAL3, 1, 0, player.dir, spr_tiles_pos ); // paleta, prioridad, vflip, hflip, tile 1
				player.sprite.posx = fix32ToInt(player.x);
				player.sprite.posy = fix32ToInt(player.y);
				VDP_setSpriteP( player_sprite, &player.sprite);
            }
            else if( keysPlayer[0] & SEGA_CTRL_LEFT && player.dir != dirLeft ){
                player.dir = dirLeft;
				
				player.sprite.tile_attr = TILE_ATTR_FULL( PAL3, 1, 0, player.dir, spr_tiles_pos ); // paleta, prioridad, vflip, hflip, tile 1
				player.sprite.posx = fix32ToInt(player.x);
				player.sprite.posy = fix32ToInt(player.y);
				VDP_setSpriteP( player_sprite, &player.sprite);
            }
        }
		// pulsado DERECHA 
		else if( keysPlayer[0] & SEGA_CTRL_RIGHT ){
			
		   if( player.state != playerJumpEnd ){
				movePlayerRight( speedRun );
				VDP_setSpritePosition( player_sprite, fix32ToInt( player.x ), fix32ToInt( player.y ));
			}
			
			player.dir = dirRight;
			player.sprite.tile_attr = TILE_ATTR_FULL( PAL3, 1, 0, player.dir, spr_tiles_pos );
			RUNANIM( playerRunRight );
        }
		// pulsado IZQUIERDA
        else if( keysPlayer[0] & SEGA_CTRL_LEFT ){
			
			if( player.state != playerJumpEnd ){
				movePlayerLeft( speedRun );
				VDP_setSpritePosition( player_sprite, fix32ToInt( player.x ), fix32ToInt( player.y ));
			}
		
			player.dir = dirLeft;
			player.sprite.tile_attr = TILE_ATTR_FULL( PAL3, 1, 0, player.dir, spr_tiles_pos );
			RUNANIM( playerRunLeft );
        }
		
		// si no hay pulsacion -> estado neutro
        else if( player.state != playerStand && player.state != playerJumpEnd ){
			
            player.frame = stand_frame;
			player.state = playerStand;
            VDP_loadTileData( spr_tiles[stand_frame], spr_tiles_pos, spr_tiles_size, 1);
        }
		
		// Si he soltado B se vuelve a permitir el salto
		if( !(keysPlayer[0] & SEGA_CTRL_B) && player.allowJump == FALSE )
			player.allowJump = TRUE;
	} 

    // salto (boton B) 
    if( keysPlayer[0] & SEGA_CTRL_B && player.allowJump == TRUE ){
		
        player.speedY = fix32Sub( player.speedY, speedJump );
		VDP_loadTileData( spr_tiles[jump1_frame ], spr_tiles_pos, spr_tiles_size, 1);
		
		player.allowJump = FALSE;

        if( keysPlayer[0] & SEGA_CTRL_RIGHT ) 
			player.state = playerJumpRight;
        else if( keysPlayer[0] & SEGA_CTRL_LEFT ) 	
				player.state = playerJumpLeft;
        else							
			 player.state = playerJump;
    }

	return(salir);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Divide 2 numeros dependiendo si es menos de 32768 (entero) o no
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int divide( int x, int d ){

	if( x < 32768 )
		return x/d;
	else
		return -1-((0-x)/d);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// mueve el jugador hacia la derecha
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
u8 movePlayerRight( fix32 speedX ){
	
    u8 collision;
	int cx;				// Control de la posicion del tile actual en el mapa
	
	collision = playerCollisionMapRight(); 
	
	if( collision == COLLISION_TILE ){
		
		if( DOWNMOVEMENT( player.speedY ) || player.state < playerJump || player.state == playerJumpEnd )
			return 1;
	}

	// Si al mover al sprite a la derecha NO hay que hacer scroll 
	if( fix32ToInt(player.x) < SCROLLXLIMIT ){
        player.x = fix32Add( player.x, speedRun );

        // Ajustamos la coordenada global del jugador
        player.mapX = fix32Add( player.mapX, speedRun );
    }
    // si no movemos el scroll
    else{
		
        // Ajustamos la coordenada global del jugador
        player.mapX = fix32Add( player.mapX, speedRun );

        if( scrollPosXPlanA < intToFix32((mapSizeX<<3)-screen_width-1)){

            // incremento o decremento la posicion del scroll
            scrollPosXPlanB = fix32Add( scrollPosXPlanB, speedScrollB );
            scrollPosXPlanA = fix32Add( scrollPosXPlanA, speedScrollA );

            // calculo el tile del mapa actual donde estamos por el scroll (primero de la izquierda)
            cx = divide( fix32ToInt(scrollPosXPlanA), 8);

            // dibujo la parte de fuera de la pantalla por la derecha si voy hacia la derecha
            // dibuja tiles del mapa desde x1 a x2 y de y1 a y2
			//VDP_updateBigTileMap( APLAN, bgA_map, cx+screen_width_tiles, 0, cx+screen_width_tiles, mapSizeY, mapSizeX, TILE_ATTR_FULL(PAL2, 1, 0, 0, bgA_tiles_pos));			
            VDP_fillBigTileMap( APLAN, bgA_map, cx+screen_width_tiles, 0, cx+screen_width_tiles, mapSizeY, mapSizeX, TILE_ATTR_FULL(PAL2, 1, 0, 0, bgA_tiles_pos));
			
			// hace el scroll
            VDP_setHorizontalScroll(BPLAN, 0, -fix32ToInt(scrollPosXPlanB));
            VDP_setHorizontalScroll(APLAN, 0, -fix32ToInt(scrollPosXPlanA));
        }
        else
            player.x = fix32Add( player.x, speedRun );
    }
	
	return 0; 
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// mueve el jugador hacia la izquierda
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
u8 movePlayerLeft( fix32 speedX ){

	u8 collision;
	u16 cx;				// Control de la posicion del tile actual en el mapa
	
	collision = playerCollisionMapLeft(); 
	
	if( collision == COLLISION_TILE ){
		
		if( DOWNMOVEMENT( player.speedY ) || player.state < playerJump || player.state == playerJumpEnd )
			return 1;
	}

    // Si al mover al sprite a la derecha NO hay que hacer scroll 
    if( player.x < intToFix32(SCROLLXLIMIT) && player.x > FIX32(-8)){
        player.x = fix32Sub( player.x, speedRun );

        // Ajustamos la coordenada global del jugador
        player.mapX = fix32Sub( player.mapX, speedRun );
    }
	// si no movemos el scroll
    else{
		
        // Ajustamos la coordenada global del jugador
        player.mapX = fix32Sub( player.mapX, speedRun );

        if( scrollPosXPlanA > FIX32(0) && fix32ToInt(player.x) <= SCROLLXLIMIT+1 ){

            // incremento o decremento la posicion
			scrollPosXPlanB = fix32Sub( scrollPosXPlanB, speedScrollB );
            scrollPosXPlanA = fix32Sub( scrollPosXPlanA, speedScrollA );

            // calculo el tile del mapa actual donde estamos por el scroll (primero de la izquierda)
            cx = divide( fix32ToInt(scrollPosXPlanA), 8 );

            // dibujo la parte de fuera de la pantalla por la izquierda si voy hacia la izquierda
			//VDP_updateBigTileMap( APLAN, bgA_map, cx, 0, cx, mapSizeY, mapSizeX, TILE_ATTR_FULL(PAL2, 1, 0, 0, bgA_tiles_pos));
			VDP_fillBigTileMap( APLAN, bgA_map, cx, 0, cx, mapSizeY, mapSizeX, TILE_ATTR_FULL(PAL2, 1, 0, 0, bgA_tiles_pos));			

            // correcion por si se pasa
            if( fix32ToInt( scrollPosXPlanA ) < 0 )
                scrollPosXPlanA = 0;

            // hace el scroll
            VDP_setHorizontalScroll( BPLAN, 0, -fix32ToInt(scrollPosXPlanB) );
            VDP_setHorizontalScroll( APLAN, 0, -fix32ToInt(scrollPosXPlanA) );
        }
        else{
            if( player.x > FIX32(-8))
                player.x = fix32Sub(player.x, speedRun);
        }
    }
	
	return 0; 
}

//////////////////////////////////////////////////////////////////////////////////////
//
//  devuelve el tile del mapa de colision de esa posicion en concreto
//
//////////////////////////////////////////////////////////////////////////////////////
u8 getTileType( s16 x, s16 y ){

    u8 tileType  = collision_map[ ( (y>>3) * mapSizeX ) + (x>>3) ];

    return tileType;
}

//////////////////////////////////////////////////////////////////////////////////////
//
//  colision con el mapa por abajo del sprite
//
//////////////////////////////////////////////////////////////////////////////////////
u8 collisionMapDown( fix32 mapX, fix32 mapY ){

    s16 ax = 0, ay = 0, bx = 0, by = 0;
    u8 tileType = 0;

    ax = fix32ToInt(mapX) +  12;	// Esquina inferior izquierda
    ay = fix32ToInt(mapY) + 32;
    bx = fix32ToInt(mapX) + 16;	// Esquina inferior derecha
    by = fix32ToInt(mapY) + 32;

    // Obtenemos los tiles de dichas coordenadas
    u16 tile_a = getTileType( ax, ay ); 
    u16 tile_b = getTileType( bx, by ); 

    // Si está pisando suelo
    if(( tile_a == COLLISION_TILE ) || ( tile_b == COLLISION_TILE ))
        tileType = COLLISION_TILE;

    return tileType;
}

//////////////////////////////////////////////////////////////////////////////////////
//
//  colision con el mapa por la derecha del sprite
//
//////////////////////////////////////////////////////////////////////////////////////
u8 collisionMapRight( s16 ax, s16 ay, s16 bx, s16 by ){

   u8 tileType = 0;

	// Obtenemos los tiles de dichas coordenadas
    u16 tile_a = getTileType( ax, ay ); 
    u16 tile_b = getTileType( bx, by ); 

    // Si está pisando suelo
    if(( tile_a == COLLISION_TILE ) || ( tile_b == COLLISION_TILE ))
		tileType = COLLISION_TILE;

    return tileType;
}

//////////////////////////////////////////////////////////////////////////////////////
//
//  colision con el mapa por la izquierda del sprite
//
//////////////////////////////////////////////////////////////////////////////////////
u8 collisionMapLeft( s16 ax, s16 ay, s16 bx, s16 by ){

    u8 tileType = 0;

	// Obtenemos los tiles de dichas coordenadas
    u16 tile_a = getTileType( ax - 8, ay ); 
    u16 tile_b = getTileType( bx - 8, by ); 

   // Si está pisando suelo
    if(( tile_a == COLLISION_TILE ) || ( tile_b == COLLISION_TILE ))
		tileType = COLLISION_TILE;
	
	return tileType;
}

