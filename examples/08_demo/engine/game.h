#ifndef _GAME_H_
#define _GAME_H_

////////////////////////////////////////////////////////////////////////////////////
// DEFINES
#define maxNumPlayers				2

// estados jugador
#define playerStand       			0
#define playerRunLeft				1
#define playerRunRight  			2
#define playerCrouch    			3
#define playerJump					4
#define playerJumpRight		   	5
#define playerJumpLeft			    6
#define playerJumpEnd			    10

#define dirRight					0
#define dirLeft						1

#define speedRun                  	FIX32(1.5)
#define speedScrollB                FIX32(0.75)
#define speedScrollA                FIX32(1.5)
#define speedJump	                FIX32(3.9)
#define gravity               	    FIX32(0.20)
#define maxFallSpeed					FIX32(3.5)
#define jumpCorrection             	FIX32(0.06)

#define animSpeedRunPlayer     		4
#define animFramesRunPlayer   		9

#define RUNANIM(dir) \
    if( player.state != (dir) ){ \
        player.state = (dir); \
        player.sprite.posx = fix32ToInt( player.x ); \
        player.sprite.posy = fix32ToInt( player.y ); \
        VDP_setSpriteP( player_sprite, &player.sprite ); } \
    if( ticks % animSpeedRunPlayer == 0 ){ \
        player.frame++; \
		if( player.frame == animFramesRunPlayer) player.frame = 1; \
        if(player.frame > 5) frame_id = 10 - player.frame; \
        else frame_id = player.frame; \
		VDP_loadTileData( spr_tiles[frame_id], spr_tiles_pos, spr_tiles_size, 1);}

#define playerCollisionMapDown() 	collisionMapDown( player.mapX, player.mapY )
#define playerCollisionMapRight() 		collisionMapRight( fix32ToInt( player.mapX )+23, fix32ToInt( player.mapY )+8, fix32ToInt( player.mapX )+23, fix32ToInt( player.mapY )+31)
#define playerCollisionMapLeft()		collisionMapLeft( fix32ToInt( player.mapX )+14, fix32ToInt( player.mapY )+8, fix32ToInt( player.mapX )+14, fix32ToInt( player.mapY )+31) 

#define tileAdjust(y) 				(y) = intToFix32((fix32ToInt(y) >> 3) << 3)

#define COLLISION_TILE      		1
#define DOWNMOVEMENT(vel_y)			((vel_y) > FIX32(0))
#define FLOORCOLLISION(c)			((c) == COLLISION_TILE) 

#define SCROLLXLIMIT           		(screen_width/2)-16
		
////////////////////////////////////////////////////////////////////
// prototipos
void gameLoop( u8 numPlayers );
u8 playerControl( u8 ticks, u8 keysPlayer[] );
u8 movePlayerLeft( fix32 speedX );
u8 movePlayerRight( fix32 speedX );
u8 collisionMapDown( fix32 mapX, fix32 mapY );
u8 collisionMapRight( s16 ax, s16 ay, s16 bx, s16 by );
u8 collisionMapLeft( s16 ax, s16 ay, s16 bx, s16 by );

////////////////////////////////////////////////////////////////////////////////////
// ESTRUCTURAS
typedef struct{

    fix32 x, y;			   				// posicion x, y en pantalla
	fix32 mapX, mapY;    		// posicion x, y global (en el mapa)
    fix32 speedX, speedY;    	// para salto y correcion del salto

	u8 frame;              			// frame de la animacion
	u8 state;            			 	// 0 stand, 1 correr iz, 2 der, 3 agachado, 4 salto normal, 5 salto derecha, 6 salto izquierda, 789 salto cayendo
	u8 dir;          					// flip, 0 derecha, 1 izquierda
    u8 allowJump;       			// para evitar el salto continuo si no se suelta el boton

    SpriteDef sprite;

}player_struct;

player_struct player;

////////////////////////////////////////////////////////////////////////////////////
// GLOBALES
// paletas para descompresion
u16 palA[16], palB[16];

// tamaño y posicion de los scrolls del mapa grande
fix32 scrollPosXPlanA, scrollPosXPlanB;

#endif
