@IF ERRORLEVEL 1 PAUSE
@echo off
@echo TEST PROGRAM
@echo Compiling.............
@set GENS_HOME=C:\dev\megadrive\

PATH=%GENS_HOME%\bin\
REM del rom.bin
make -f makefile.gen

pause

gens.exe %GENS_HOME%\examples\01_hola_mundo\rom.bin

del *.out
del *.o
del *.lst
del *.o80
del z80_xgm.h
del *.s